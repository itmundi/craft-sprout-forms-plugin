<?php
namespace Craft;

class SproutForms_EntriesExportService extends BaseApplicationComponent implements IExportElementType
{
    /**
     * Return export template.
     *
     * @return string
     */
    public function getTemplate()
    {
        return 'sproutforms/export/_entry';
    }

    /**
     * Get form groups.
     *
     * @return array|bool
     */
    public function getGroups()
    {
        return sproutForms()->forms->getAllForms();
    }

    /**
     * Return entry fields.
     *
     * @param array $settings
     * @param bool  $reset
     *
     * @return array
     */
    public function getFields(array $settings, $reset)
    {
        // Get form id
        $formId = $settings['elementvars']['form'];
        $form = sproutForms()->forms->getFormById($formId);

        // Get entries
        $criteria = craft()->elements->getCriteria('SproutForms_Entry');
        $criteria->formId = $form;

        // Get form entry tabs
        $tabs = craft()->fields->getLayoutById($form->fieldLayoutId)->getTabs();

        // Create a field map
        $fields = array();

        // Set the dynamic fields for this form type
        foreach ($tabs as $tab) {
            $fieldData = array();
            foreach ($tab->getFields() as $field) {
                $data = $field->getField();
                $fieldData[$data->handle] = array(
                    'name' => $data->name,
                    'checked' => 1,
                );
            }
        }

        // Return fields
        return $fieldData;
    }

    /**
     * Set entry criteria.
     *
     * @param array $settings
     *
     * @return ElementCriteriaModel
     */
    public function setCriteria(array $settings)
    {

        // Get entries by criteria
        $criteria = craft()->elements->getCriteria('SproutForms_Entry');
        //$criteria->order = 'id '.$settings['sort'];
        $criteria->offset = $settings['offset'];
        $criteria->limit = $settings['limit'];
        $criteria->status = isset($settings['map']['status']) ? $settings['map']['status'] : null;

        // Get by section and entrytype
        $criteria->formId = $settings['elementvars']['form'];

        return $criteria;
    }

    /**
     * Get entry attributes.
     *
     * @param array            $map
     * @param BaseElementModel $element
     *
     * @return array
     */
    public function getAttributes(array $map, BaseElementModel $element)
    {
        $attributes = array();

        // Try to parse checked fields through prepValue
        foreach ($map as $handle => $data) {
            if ($data['checked'] && !strstr($handle, ExportModel::HandleTitle)) {
                $attributes[$handle] = $element->$handle;
            }
        }

        return $attributes;
    }
}
